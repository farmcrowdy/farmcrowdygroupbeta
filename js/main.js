document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    
    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
    
      // Add a click event on each of them
      $navbarBurgers.forEach( el => {
        el.addEventListener('click', () => {
    
          // Get the target from the "data-target" attribute
          const target = el.dataset.target;
          const $target = document.getElementById(target);
    
          // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
          el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
    
        });
      });
    }
    
    });
     //open modal for team page
     let modal_body = document.querySelectorAll('.modal')
     $('.team-box').click(function(){
        let id = $(this).attr('id');
        for(let modal = 0; modal < modal_body.length; modal++){
         let vard = modal_body[modal];
         let dataAttr = vard.getAttribute("data");
         if(id === dataAttr){
           $("." + dataAttr).addClass("is-active"); 
         }            
       }
     })
     $(".close_modal").click(function() {
        $(".modal").removeClass("is-active");
     });